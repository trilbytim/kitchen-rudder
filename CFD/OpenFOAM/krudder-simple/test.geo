boxlen = 3.5;
boxwidth = 1.5;


lc = 40e-3;
lcfine = 4e-3;

//Box
Point(1) = {0,0,0, lc};
Point(2) = {boxlen,0,0, lc};
Point(3) = {boxlen,boxwidth,0, lc};
Point(4) = {0,boxwidth,0, lc};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(1) = {1, 2, 3,4};
Plane Surface(1) = {1};

newEntities =
Extrude {0, 0, 0.01}
{
  Surface{1}; Layers{1}; Recombine;
};


Physical Volume(100) = {newEntities[1]};
Physical Surface("frontAndBack") = {1,newEntities[0]};
Physical Surface("fixedWalls") = {newEntities[3],newEntities[4],newEntities[5]};
Physical Surface("movingWall") = {newEntities[2]};



