boxlen = 3.5;
boxwidth = 1.5;
swimwidth = 0.9;
swimlength = 0.77;

propdia = 0.457;
propthick = 20e-3;
propxpos = swimlength+0.47;
// Tiny misalignment:
propypos = boxwidth/2;// + boxwidth/1000;

lc = 20e-3;
lcfine = 2e-3;

//Box
Point(1) = {-propxpos,-propypos,0, lc};
Point(2) = {boxlen-propxpos,-propypos,0, lc};
Point(3) = {boxlen-propxpos,boxwidth-propypos,0, lc};
Point(4) = {-propxpos,boxwidth-propypos,0, lc};
Point(41) = {-propxpos,swimwidth/2,0, lcfine};
Point(42) = {-propxpos+swimlength,0,0, lcfine};
Point(43) = {-propxpos,-swimwidth/2,0, lcfine};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 41};
Line(41) = {41, 42};
Line(42) = {42, 43};
Line(43) = {43, 1};


//Prop
//Point(5) = {propthick/2,propdia/2,0,lcfine};
//Point(6) = {-propthick/2,propdia/2,0,lcfine};
//Point(7) = {-propthick/2,-propdia/2,0,lcfine};
//Point(8) = {propthick/2,-propdia/2,0,lcfine};



//Line(5) = {5,6};
//Line(6) = {6,7};
//Line(7) = {7,8};
//Line(8) = {8,5};



Line Loop(1) = {4, 41,42,43,1, 2, 3};
//Line loop(2) = {5,6,7,8};
Plane Surface(1) = {1};

newEntities =
Extrude {0, 0, 0.01}
{
  Surface{1}; Layers{1}; Recombine;
};



fluid = 1;   front = 2; swimtop = 3; swimbot = 4; //prop = 2;
propIn = 23; propOut = 24; propTop = 25; propBot = 26;

Physical Volume(100) = {newEntities[1]};
Physical Surface("frontAndBack") = {newEntities[0],1};
//Physical Surface("front") = {newEntities[0]};
Physical Surface("swim") = {newEntities[3],newEntities[4]};
Physical Surface("domainBoundary") = {newEntities[2],newEntities[5],newEntities[6],newEntities[7],newEntities[8]};
//Physical Surface(prop) = 2;
//Physical Line(swimtop) = 41;
//Physical Line(swimbot) = 42;
//Physical Line(propIn) = 6;
//Physical Line(propOut) = 8;
//Physical Line(propBot) = 7;
//Physical Line(propTop) = 5;


