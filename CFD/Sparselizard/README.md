# Kitchen rudder

To design a kitchen rudder for a narrowboat. This consists of a Jupyter Notebook to generate the mesh files and call Sparselizard.

## Getting started
Since Sparselizard can only read older formats of mesh created by Gmsh, you must add the lines:

<CODE>Mesh.Format = 1; // msh output format
Mesh.MshFileVersion = 2.2; // Version of the MSH file format to use</CODE>


To the bottom of your .gmshrc file that is in your home directory.
