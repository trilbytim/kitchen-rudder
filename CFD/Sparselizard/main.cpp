//Code to simulate flow from a propeller through a kitchen rudder.


#include "sparselizard.h"


using namespace sl;

int main(void)
{	
    // Region numbers used in this simulation as defined in the .msh file:
    int fluid = 1, swimtop = 2, swimbot = 3, propIn = 4, propOut = 5, rud1 = 6,rud2 = 7,rud3 =8,rud4 =9;
    
    // Load the mesh (GMSH format):
    mesh mymesh("krudder.msh");
    
    // Field v is the flow velocity. It uses nodal shape functions "h1" with two components in 2D.
    // Field p is the relative pressure.
    field v("h1xy"), p("h1"), y("y");
    int rudskin1 = selectintersection({fluid,rud1});
    int rudskin2 = selectintersection({fluid,rud2});
    int rudskin3 = selectintersection({fluid,rud3});
    int rudskin4 = selectintersection({fluid,rud4});
    
    // Force a x-direction flow velocity from the prop:
    double h = 0.4826/2;
    p.setconstraint(propOut, (h*h-y*y));
    p.setconstraint(propIn, -(h*h-y*y));

    
    v.setconstraint(swimtop);
    v.setconstraint(swimbot);
    v.setconstraint(rudskin1);
    v.setconstraint(rudskin2);
    v.setconstraint(rudskin3);
    v.setconstraint(rudskin4);

    // Use an order 1 interpolation for p and 2 for v on the fluid region (satisfies the BB condition):
    p.setorder(fluid, 1); v.setorder(fluid, 2);
    
    // Dynamic viscosity of water [Pa.s] and density [kg/m3] at room temperature and atmospheric pressure:
    double mu = 1.0518e-3, rho = 1000;
    
    // Define the weak formulation for time-dependent incompressible laminar flow:
    formulation laminarflow;
    
    laminarflow += integral(fluid, predefinednavierstokes(dof(v), tf(v), v, dof(p), tf(p), mu, rho, 0, 0, true) );
    
    // Define the object for an implicit Euler time resolution.
    // The initial field values are taken as the fields are.
    // An all zero initial time derivative is set with 'vec(laminarflow)'.
    impliciteuler eul(laminarflow, vec(laminarflow));
    // Set the relative tolerance on the inner nonlinear iteration:
    eul.settolerance(1e-4);
    
    // Run from 0 sec to 90 sec by steps of 0.2 sec (450 timesteps):
    settime(0);
    for (int i = 0; i < 10; i++)
    {
        // Compute one timestep with an unlimited number of nonlinear iterations (-1):
        eul.next(1, -1);
        
        // Write field v with an order 2 interpolation to ParaView .vtu format:
        v.write(fluid, "output/v" + std::to_string(1000 + i) + ".vtu", 2);
        p.write(fluid, "output/p" + std::to_string(1000 + i) + ".vtu", 2);
    }
}

